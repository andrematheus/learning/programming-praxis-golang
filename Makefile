export GOPATH := $(shell pwd):$(GOPATH)
export GOBIN := $(shell pwd)/bin

build:
	go build programmingpraxis/rpncalculator

test:
	go test programmingpraxis/rpncalculator

install:
	go install programmingpraxis/rpncalculator

clean:
	rm -f rpncalculator
	rm -fr bin